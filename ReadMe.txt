Rsync is good. BackupPC is good. Rsync on Windows is...not so good. We (small-ish IT department) decided on using the DeltaCopy version of rsync (still using Cygwin) to allow our BackupPC server(s) to backup our windows clients. It's fairly quick'n dirty but works well enough for our purposes.

Reminder that deltacd.conf is basically /etc/rsyncd.conf

Note that DeltaCopy is distributed with an older version of Cygwin and Rsync. At some point we updated the versions of cygwin (1.7.5) and rsync (3.0.7). Those have continued to work for our needs, so we have not tested with anything newer.

DeltaCopy, Rsync and Cygwin are all distributed under GPLv3, access their respective source at

http://www.aboutmyip.com/AboutMyXApp/DeltaCopy.jsp
https://rsync.samba.org/
https://cygwin.com/

The redistributed compiled code is Copyright of their respective creators under the GPL.

This collection of scripts and text bits and bobs are likewise:
Copyright (C) 2011, 2015 by Matthew Parsons under the terms of the GNU General Public License. (http://www.gnu.org/licenses/) It may be redistributed as long as all attribution and this notice is maintained.
