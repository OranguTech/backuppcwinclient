@echo off
REM Install DeltaCopy(Rsync for Windows http://www.aboutmyip.com/AboutMyXApp/DeltaCopy.jsp) for use by backupPC (http://backuppc.sourceforge.net/).
REM Copyright 2011 - Matthew Parsons
REM Ver 1.0.4


REM Change to batchfile working directory. Supposed to work with UNC paths, but doesn't.
REM cd /d %~dp0

REM 1.0.2 - Uses pushd to work w/ UNC paths:

pushd "%~dp0"



REM cls && mode 55,5
color 40
title Install BackupPC - Admin Reminder
echo Remember, this must be run as an admin/elevated prompt!
echo Port 873 will need to be open on the firewall!
REM (You better be using Group Policy!)
pause

color
title Installing BackupPC for Windows
REM cls && mode 55,40 && color 02


REM Create BackupPC user. Recommended to manage user via Active Directory.
echo Creating BackupPC user...
net user backuppc !ThisIsALocalPassword! /add /active:yes /comment:"Used by Backuppc/Deltacopy Service" /expires:never 

REM work-around for net user /expires bug - http://www.tomshardware.com/forum/251587-45-password-expires-user-account-trough-user
wmic useraccount where name='backuppc' set PasswordExpires=FALSE


Echo adding user to Backup Operators group...
net localgroup "Backup Operators" backuppc /add
echo adding user to admin group...
net localgroup Administrators backuppc /add

pause
REM Give backuppc user login as service rights. Uses ntrights.exe from Server2003 Resource Kit tools.

echo giving user ServiceLogon rights
.\ntrights.exe +r SeServiceLogonRight -u backuppc
REM remove local login right
.\ntrights.exe -r SeInteractiveLogonRight -u backuppc


pause

echo copying DeltaCopy files...
REM Copy Files
xcopy .\DCSource\*.* c:\DeltaCopy\ /E /Y


REM - probably needs handler for 32 vs 64 bit.
echo Importing (Stupidly) needed registry entry.
regedit /s .\Deltacopy.reg
regedit /s .\Deltacopy32.reg

pause


echo  Copying conf file...

REM Figure out which .conf to use. Went with existence of profile path over OS version 'cause that's what we really care about.
REM Yes it is inexact and there could be edge cases.

REM Put Vista/2008/7 file in default.
REM if exist c:\Users\ goto NewerProfile

if exist "c:\Users\" goto NewerProfile

if exist "c:\Documents and Settings\" goto OlderProfile

goto NoProfile

:NewerProfile
echo Configuring for Vista/2008/7
copy .\deltacd.conf.win7 c:\DeltaCopy\deltacd.conf /y

goto confdone


:OlderProfile
echo Configuring for XP
copy .\deltacd.conf.xp c:\DeltaCopy\deltacd.conf /y

goto confdone

:NoProfile
color 40
Echo "I can't find the UserProfile folder!"
Echo "You'll need to figure out what went wrong and set up c:\DeltaCopy\deltacd.conf"

pause


:confdone


REM Register Service. (http://support.microsoft.com/kb/251192)

Echo Registering DCService...
sc create DeltaCopyservice binPath= c:\DeltaCopy\DCService.exe DisplayName= "DeltaCopy Server" type= own start= auto obj= ".\backuppc" 

echo Setting DCService password...
sc config DeltaCopyservice password= "!ThisIsALocalPassword!"

echo ======
echo Starting DCService...
sc start DeltaCopyservice
echo ======
Echo DeltaCopy/BackupPC should be installed now.

REM Unmount UNC/Z
popd


Pause

REM Copyright 2011, 2015 under the terms of the GNU General Public License. (http://www.gnu.org/licenses/) It may be redistributed as long as all attribution and this notice is maintained.
